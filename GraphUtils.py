import numpy
import scipy
import scipy.stats
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def scatter(xvalues, yvalues, xlabel, ylabel, filename, regression=None):

    try:

        plt.subplots()

        plt.scatter(xvalues, yvalues, color='b')

        if regression is not None:

            lsp = numpy.linspace(min(xvalues), max(xvalues))

            if regression == 'linear':

                slope, intercept, Rsq, pvalue, stderr = scipy.stats.linregress(xvalues, yvalues)

                reg_values = [slope * pos + intercept for pos in lsp]

                equation_str = '%0.3f*t + %0.2f' % (slope, intercept)

                equation_label = 'Linear'

            elif regression == 'exponential':

                def test_func(x, a, b, c):
                    return a * numpy.exp(b * x) + c

                popt, pcov = curve_fit(test_func, numpy.asarray(xvalues), numpy.asarray(yvalues), p0=[6, 0.3, 5])

                a = popt[0]
                b = popt[1]
                c = popt[2]

                if c > 0:
                    equation_str = '%i exp(%0.3f*t) + %i' % (int(a), b, int(c))
                else:
                    equation_str = '%i exp(%0.3f*t) - %i' % (int(a), b, int(c) * -1)
                equation_label = 'Exponential'

                ybar = numpy.mean(yvalues)
                sstot = 0
                ssres = 0

                for x, y in zip(xvalues, yvalues):
                    sstot = sstot + (test_func(x, a, b, c) - ybar) ** 2
                    ssres = ssres + (test_func(x, a, b, c) - y) ** 2

                Rsq = 1 - ssres / sstot

                reg_values = [popt[0] * numpy.exp(popt[1] * x) + popt[2] for x in lsp]

            elif regression == 'logistic':

                def test_func(x, mu, s):
                    return 1 / (1 + numpy.exp(-(x - mu) / s))

                popt, pcov = curve_fit(test_func, numpy.asarray(xvalues), numpy.asarray(yvalues), p0=[5, 1])

                mu = popt[0]
                s = popt[1]

                equation_str = '1/(1+e^(-(x-%0.2f)/%0.2f)' % (mu, s)
                equation_label = 'Logistic, midpoint: %0.2f' % mu

                ybar = numpy.mean(yvalues)
                sstot = 0
                ssres = 0

                for x, y in zip(xvalues, yvalues):
                    sstot = sstot + (test_func(x, mu, s) - ybar) ** 2
                    ssres = ssres + (test_func(x, mu, s) - y) ** 2

                Rsq = 1 - ssres / sstot

                reg_values = [test_func(x, mu, s) for x in lsp]
            else:
                raise ValueError('Unknown regression type')

            plt.annotate(equation_str, (0.05, 0.9), xycoords='axes fraction')
            plt.annotate('Rsq = {0:.3f}'.format(Rsq), (0.05, 0.85), xycoords='axes fraction')
            plt.plot(lsp, reg_values, color='r', label=equation_label, linewidth=3)

        plt.axis('tight')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise


def stackedbar(value_keys,
               dict_array,
               xtick_labels,
               xlabel,
               ylabel,
               filename,
               rotation='horizontal',
               mapper=None,
               cmap='viridis'):

    color_mapper = get_cmap(len(value_keys), cmap=cmap)

    if mapper is None:
        mapper = {}

    try:

        plt.subplots()

        index = numpy.arange(len(xtick_labels))
        bar_width = 0.70

        opacity = 0.8
        previous_top = []
        counter = 0

        patch_array = []

        for key in value_keys:

            data_array = []

            for data_dict in dict_array:
                # extract the data in the specified order

                value = data_dict.get(key, 0)

                data_array.append(value)

            if len(previous_top) == 0:
                previous_top = [0] * len(data_array)

            plt.bar(index,
                    data_array,
                    bar_width,
                    alpha=opacity,
                    color=color_mapper(counter),
                    align='center',
                    bottom=previous_top)

            # need to keep track of where we are on the plot.
            for i in range(0, len(data_array)):
                previous_top[i] = previous_top[i] + data_array[i]

            # artist = mpatches.Patch(color=color_mapper(counter), label=mapper.get(key, str(key).upper()))
            # patch_array.append(artist)

            counter = counter + 1

        plt.axis([0-bar_width, len(dict_array)-bar_width/2, 0, 1.0])

        """
        plt.legend(handles=patch_array,
                   loc='lower center',
                   bbox_to_anchor=(0.5, min(-0.036 * float(len(value_keys)), -0.3)), ncol=3)
                   
        """

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        # plt.xticks(index, [mapper.get(k, k) for k in xtick_labels], rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')
    except:
        plt.close('all')
        raise


def get_cmap(N, cmap):
    import matplotlib.cm as cmx
    import matplotlib.colors as colors
    '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
    RGB color.'''
    color_norm = colors.Normalize(vmin=0, vmax=N - 1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap=cmap)

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)

    return map_index_to_rgb_color
